#define new string variables
days = "Mon Tue Wed Thu Fri Sat Sun"
#the '\n' means start on a new line
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

#print each variable
puts "Here are the days: #{days}"
puts "Here are the months: #{months}"

#This allows the user to print text, across multiple lines, to the screen
puts %q{
There's something going on here
With this weird quote
We'll be able to type as much as we like.
Even 4 lines if we want, or 5, or 6
}