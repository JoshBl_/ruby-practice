#define new variables with various different escape sequences
tabby_cat = "\tI'm tabbed in"
persian_cat = "I'm split\non a line"
backslash_cat = "I'm a \\ a \\ cat"

#using triple quotes - can put in as many lines of text as I want, until the next set of triple quotes!
fat_cat = """
I'll do a list:
\t* Cat food
\t* Fishies
\t* Catnip \n\t* Grass
"""

#print the value of each variables
#shows string that is tabbed
puts tabby_cat
#shows string that is split across two lines
puts persian_cat
#shows string where a backslash is printed before and after 'a'
puts backslash_cat
#triple quotes allows as many lines of text until next set of triple quotes
puts fat_cat