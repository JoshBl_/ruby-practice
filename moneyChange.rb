puts """Welcome to the money exchange!
We give back 10% of whatever you put in!
Now, give us some money!
"""
money = gets.chomp.to_f
change = money / 10

puts """You gave us #{money}
And we will give you back 10%
Which is.... #{change}
"""