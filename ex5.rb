#declared variables
name = 'Joshua Blewitt'
age = 26
height = 74
weight = 180
eyes = "blue"
teeth = "white"
hair = "brown"

#using said variables in statements
puts "Lets talk about #{name}"
puts "He's #{height} inches tall"
puts "In centimeters that's #{height * 2}!"
puts "He's #{weight} pounds"
puts "In kilograms that's #{weight / 2}!"
puts "That's a little too heavy!"
puts "He's got #{eyes} eyes and #{hair} hair"
puts "His teeth are usually #{teeth} depending on the coffee"

puts "If I add #{age}, #{height}, and #{weight} I get #{age + height + weight}."