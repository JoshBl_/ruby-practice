#variable user_name value is the first argument 
user_name = ARGV.first
prompt = "> "

puts "Hi #{user_name}"
puts "I'd like to ask you a few questions."
puts "Do you like me, #{user_name}?"
puts prompt
#new variables called likes, using $stdin.get.chomp
likes = $stdin.gets.chomp

puts "Where do you live, #{user_name}"
puts prompt
lives = $stdin.gets.chomp

#using a ',' at the end of puts is basically using puts twice
puts "What kind of computer do you have? ", prompt
computer = $stdin.gets.chomp

puts """
Alright, so you said #{likes} about liking me
You live in #{lives}. Not sure where that is.
And you have a #{computer} computer. Nice
"""