#define variables and values
#integer
cars = 100
#floating point number
space_in_a_car = 4.0
#integer
drivers = 30
#integer
passengers = 90
#using the variables above, calculate new values for new variables
cars_not_driven = cars - drivers
cars_driven = drivers
carpool_capactiy = cars_driven * space_in_a_car
average_passengers_per_car = passengers / cars_driven

#print the values of the variables to the screen
puts "There are #{cars} cars available"
puts "There are only #{drivers} today"
puts "There will be #{cars_not_driven} empty cars today"
puts "We can transport #{carpool_capactiy} people today"
puts "We have #{passengers} to carpool today"
puts "We need to put about #{average_passengers_per_car} in each car"