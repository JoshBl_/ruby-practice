#create new variable called formatter
formatter = "%{first} %{second} %{third} %{fourth}"

#prints the value assigned to the formatter
puts formatter % {first: 1, second: 2, third: 3, fourth: 4}
puts formatter % {first: "one", second: "two", third: "three", fourth: "fourth"}
puts formatter % {first: true, second: false, third: false, fourth: false}
puts formatter % {first: formatter, second: formatter, third: formatter, fourth: formatter}

puts formatter % {
    first: "I had this thing",
    second: "That you could type up right",
    third: "But it didn't sing",
    fourth: "So I said goodnight."
}
