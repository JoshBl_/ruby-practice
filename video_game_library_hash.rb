#creating a new blank hash
library = Hash.new

#method for adding a game
def addGame(hash)
    #boolean declared for while loop
    valueCheck = true
    while valueCheck == true
        puts "Enter the name of the game"
        #get user input
        gameName = gets.chomp
        #if statement to check if the game name from the user already exists in the hash
        if hash.has_value?(gameName) == true
            #if it exists - print message and loop
            puts "This already exists! Try again!"
        else
            #if it doesn't exist, run the following
            #new integer declared
            testCount = 0
            #iterate through the hash, for every key, value pair it finds, increment testCount by 1
            hash.each {|key, value| testCount += 1}
            #increment testCount by 1 to ensure that the key being added to the hash is unique
            testCount += 1
            puts "Adding #{gameName} to library with an ID of #{testCount}"
            #add the new game to the hash
            hash[testCount] = "#{gameName}"
            puts "Done!"
            #set boolean to false, ending the loop
            valueCheck = false
            puts "Press enter to return to the main menu"
            gets.chomp
        end
    end
end

#method for viewing the library
def viewLibrary(hash)
    puts "View library"
    #if the hash is empty, print a message
    if hash.empty? == true
        puts "There are no games! Please add a game!"
        puts "Press enter to go back to the main menu"
        gets.chomp
    else
        #if the hash is populated, do the following
        #new integer declared
        gameCount = 0
        #iterate through the hash, for every key, value pair it finds, increment variable by 1
        hash.each {|key, value| gameCount += 1}
        puts "You currently have #{gameCount} games in your library."
        #iterate through the hash, for every key, value pair it finds - print them to the screen
        hash.each {|key, value| puts "#{key} - #{value}"}
        puts "Press enter to go back to the main menu"
        gets.chomp
    end
end

#method for clearing the library
def clearLibrary(hash)
    puts "WARNING! This will remove every game from the library, this can't be undone!"
    puts "Do you wish to continue?"
    #boolean declared
    clearLoop = true
    #while loop
    while clearLoop == true
        puts "Type 'yes' to clear the library. Type 'no' to cancel."
        confirm = gets.chomp.upcase
        #case statement based on what the user enters
        case confirm
        when "YES"
            #hash is cleared
            hash.clear
            #boolean is set to false
            clearLoop = false
            puts "The library has been cleared!"
            puts "Press enter to return to the main menu"
            gets.chomp
        when "NO"
            #boolean is set to false
            clearLoop = false
            puts "Clear cancelled"
            puts "Press enter to return to the main menu"
            gets.chomp
        else
            puts "Error! Unknown command!"
        end
    end
end

#method for writing to file
def writeToFile (hash)
    puts "The current hash will be written to a file. If you have previously written to a file before, this data will be overwritten."
    #boolean declared
    writeLoop = true
    #while loop
    while writeLoop == true
        puts "Do you want to continue? Type 'Yes' to write to file or 'No' to cancel."
        writeChoice = gets.chomp.upcase
        case writeChoice
        when "YES"
            puts "Please state the location of where you want 'library.txt' written to."
            location = gets.chomp
            #specifies the file to open
            target = open("#{location}" + '\\library.txt', 'w')
            #iterates through the hash and for every key, value pair it finds - print to the file.
            hash.each {|key, value| target.write("#{key} - #{value}\n")}
            #close the file
            target.close
            #set the boolean to false
            writeLoop = false
            puts "File written! File is called library.txt"
            puts "Press enter to go back to the main menu"
            gets.chomp
        when "NO"
            puts "Press enter to go back to the main menu."
            gets.chomp
            writeLoop = false
        #else statement for catching unknown commands
        else
            puts "Error! Unknown command!"
        end
    end
end

#method for flatten the hash to an array
def flattenArray(hash)
    #boolean
    newLoop = true
    puts "This will take the current hash (the library) and return an array."
    #while loop
    while newLoop == true
        puts "Do you wish to continue? (Type 'YES' or 'NO')"
        flatConfirm = gets.chomp.upcase
        case flatConfirm
        when 'YES'
            puts "Flattening the hash - returning an array"
            #new variable defined which contains the result of when the hash is flattened
            flattenHash = hash.flatten
            #print the variable to the screen
            puts flattenHash
            #set the loop to false
            newLoop = false
            puts "Press enter to return to the main menu"
            gets.chomp
        when 'NO'
            newLoop = false
            puts "Press enter to return to the main menu"
            gets.chomp
        else
            puts "Error! Unknown command!"
        end
    end
end

puts "Welcome to the video game library!"

#boolean declared for while loop
check = true

#while loop with a case statement to act as a menu
while check == true
    puts "What would you like to do? Enter the first letter of a menu choice!"
    puts "(V)iew library"
    puts "(A)dd games to the library"
    puts "(C)lear library"
    puts "(W)rite to file"
    puts "(F)latten to an array"
    puts "(Q)uit"
    #take the user choice and convert to uppercase - assigned to choice
    choice = gets.chomp.upcase
    case choice
    when 'V'
        viewLibrary(library)
    when 'A'
        #calls the addGame method and passes the hash library as an argument
        puts "Add game"
        addGame(library)
    when 'C'
        #calls the clearLibrary method and passes the hash library as an argument
        puts "Clear library"
        clearLibrary(library)
    when 'W'
        #calls the writeToFile method and passes the hash library as an argument
        puts "Write to file"
        writeToFile(library)
    when 'F'
        #calls the flattenArray method and passes the hash library as an argument
        puts "Flatten to an array"
        flattenArray(library)
    when 'Q'
        #quits application
        puts "Goodbye!"
        check = false
    else
        #else statement to catch errors
        puts "Error! Unknown command."
    end
end
