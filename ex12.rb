#asks user for a number, converts response to an integer using 'to.i' at the end of gets.chomp
print "Give me a number: "
number = gets.chomp.to_i

#the value of bigger is the value of number times 100
bigger = number * 100
puts "A bigger number is #{bigger}."

#asks user for another number, and stores it as a string.
print "Give me a another number: "
another = gets.chomp
#then the value of number is changed based upon the value of another (which is converted to an integer)
number = another.to_i

#the value of smaller is the value of number divided by 100
smaller = number / 100
puts "A smaller number is #{smaller}."

#similar as before, but this time - converts the response to a float!
print "I want another number: "
newNum = gets.chomp.to_f

puts "That new number is #{newNum}"