#print and ask for values from user
print "How old are you? "
#gets is to get user input
age = gets.chomp
print "How tall are you? "
#chomp - cuts off line break!
height = gets.chomp
print "How much do you weigh? "
weight = gets.chomp

#print the values to the user
puts "So, you're #{age} old, #{height} tall, and #{weight} heavy."

print "By the way, do you like the Magic The Gathering card game? "
response = gets.chomp

print "You said, #{response}"