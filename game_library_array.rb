#new project - purpose is to look at pushing new elements to an array

#defining a blank array
library = []

def viewLibrary(array)
    if (array.count == 0)
        puts "There are no games in your library! Add some games!"
        puts "Press enter to return to the menu"
        gets.chomp
    else
        arrayGameCount = array.count.to_s
        puts "There are currently " + arrayGameCount + " games in your library"
        puts "Here are the games in your library:"
        puts array
        puts "Press enter to return to the menu."
        gets.chomp
    end
end

def addToLibrary(array)
    puts "Adding a game to the library will be split into three parts; the name, the platform and the year."
    puts "Please enter the name of the game you want to add."
    gameName = gets.chomp
    puts "Please enter the platform for the game."
    gamePlatform = gets.chomp
    puts "Please enter the year that the game was released."
    gameYear = gets.chomp
    puts "Now adding #{gameName} - #{gamePlatform} - #{gameYear} to the game library."
    array.push("#{gameName} - #{gamePlatform} - #{gameYear}")
    puts "Game added to the library!"
    puts "Press enter to return to the menu."
    gets.chomp
end

valid = true

while valid == true
    puts "Select a menu choice by typing in the character between the brackets!"
    puts "(V)iew games"
    puts "(A)dd games to library"
    puts "(S)earch library"
    puts "(Q)uit"
    choice = gets.chomp.upcase

    case choice
    when 'V'
        puts "View games in Library"
        viewLibrary(library)

    when 'A'
        puts "Add games to library"
        addToLibrary(library)

    when 'S'
        puts "Search library"
        searchLibrary(library)

    when 'Q'
        puts 'Quit'
        valid = false
    else
        puts "Error"
    end
end

    