puts "Let's practice everyhing."
puts "You\'d need to know \'bout escapes with \\ that do \n newlines and \t tabs"

#the <<END is a "heredoc"
#used to create a multi-line string
#you start it with '<<' and then an all caps word (any word)
#Ruby then reads everything into a string
poem = <<END
\tThe lovely world
with logic so firmly planted
cannot discern \n in the needs of love
nor comprehend passion from intuition
and requires an explanation
\n\t\twhere there is none.
END

puts "--------------"
#prints the poem string
puts poem
puts "--------------"

five = 10 - 2 + 3 - 6
puts "This should be give: #{five}"

def secret_formula(started)
    jelly_beans = started * 500
    jars = jelly_beans / 1000
    crates = jars / 100
    return jelly_beans, jars, crates
end

start_point = 10000
beans, jars, crates = secret_formula(start_point)

puts "With a starting point of: #{start_point}"
puts "We'd have #{beans} beans, #{jars} jars, and #{crates} crates."

#a C style of inserting variables into Ruby strings, optional to use
start_point = start_point / 10
puts "We can also do that this way:"
puts "We'd have %s beans, %d jars, and %d crates." % secret_formula(start_point)