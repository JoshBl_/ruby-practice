#new argument variables
from_file, to_file = ARGV

puts "Copying from #{from_file} to #{to_file}"

#new variables, one to open the file and one to read the file
in_file = open(from_file)
indata = in_file.read

#calculate the size of the file
puts "The input file is #{indata.length} bytes long"

#checking to make sure that the destination file exists
puts "Does the output file exist? #{File.exist?(to_file)}"
puts "Ready, hit RETURN to continue, CTRL-C to abort."
$stdin.gets

#copy the file
out_file = open(to_file, "w")
out_file.write(indata)

puts "Alright, all done"

#close the files
out_file.close
in_file.close