#we interact with this file using the irb interpreter from the terminal
#modules are a way of grouping together methods, classes and constants. They provide a namepsace and prevent name clashes!
module Ex25
   #function will break up words 
   def Ex25.break_words(stuff)
    words = stuff.split(' ')
    return words
   end

   #function to sort words
   def Ex25.sort_words(words)
    #sort works by comparing elements of a collection using spaceship (<=>) operator, using the quicksort algorithm
    #quicksort = choose a pivot, put everything smaller or equal to pivot to the left, everything larger to the right. For each sublist choose a pivot and do the same
    return words.sort
   end

   #prints the first word after shifting it off
   def Ex25.print_first_word(words)
    #removes the first element of an array and returns it
    word = words.shift
    puts word
   end

   #prints the last word after popping it off
   def Ex25.print_last_word(words)
    #removes the last element of an array and returns it
    word = words.pop
    puts word
   end

   #takes in a full sentence and returns sorted words
   def Ex25.sort_sentence(sentence)
    #pass the sentence variable as an argument for the break_words function
    words = Ex25.break_words(sentence)
    #pass the words variable (above) as an argument for the sort_words function
    return Ex25.sort_words(words)
   end

   #prints the first and last words of a sentence
   def Ex25.print_first_and_last(sentence)
    #pass the sentence variable to the break_words function. Value is assigned to words
    words = Ex25.break_words(sentence)
    #pass the words variable as the argument for print the first word and last word functions
    Ex25.print_first_word(words)
    Ex25.print_last_word(words)
   end

   #sorts words then prints first and last
   def Ex25.print_first_and_last_sorted(sentence)
    #pass the sentence variable to the sort_sentence function. Value is assigned to words
    words = Ex25.sort_sentence(sentence)
    #pass the words variable as the argument for print the first word and last word functions
    Ex25.print_first_word(words)
    Ex25.print_last_word(words)
   end
end