#declare variables
types_of_people = 10
x = "There are #{types_of_people} types of people"
binary = "binary"
do_not = "don't"
y = "Those who know #{binary} and those who #{do_not}."

#display x and y
puts x
puts y

#print variables
puts "I said #{x}"
puts "I also said : #{y}"

#more variables!
hilarious = false
joke_evaluation = "Isn't that joke so funny?! #{hilarious}"

#you can use a single quote as well, it lets you use a double quote inside! (and vice versa)
puts joke_evaluation
w = 'This is the left side of...'
e = "a string with a right side"

#display variable w and e together
puts w + e