#needing name of file when program is called as an argument
input_file = ARGV.first

#function to print the file
def print_all(f)
    puts f.read
end

def rewind(f)
    #seek = attempt to find a given a position as an integer.
    #in this case, position 0 - the beginning of the file!
    f.seek(0)
end


def print_a_line(line_count, f)
    #in this case, gets is used to get a line of text, chomp to remove the line break
    puts "#{line_count}, #{f.gets.chomp}"
end

current_file = open(input_file)

puts "First let's print the whole file:\n"
#calls print all function - passing the file specified by the user as the argument
print_all(current_file)

puts "Now let's rewind, kind of like a tape"
#seeks the first place in the file (as seek has the integer value of 0)
rewind(current_file)

puts "Let's print three lines:"

#prints a line and then the current line value is incremented by 1 each time
current_line = 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)

current_line += 1
print_a_line(current_line, current_file)