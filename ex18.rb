#function with ARGV
def print_two(*args)
    arg1, arg2 = args
    puts "arg1: #{arg1}, arg2: #{arg2}"
end

#functions start with def, and finish with end!
def print_two_again (arg1, arg2)
    puts "arg1: #{arg1}, args2: #{arg2}"
end

#this function takes one argument
def print_one(arg1)
    puts "arg1: #{arg1}"
end

#this function takes no arguments
def print_none()
    puts "I got nothin'"
end

#call the functions with valid arguments to use them!
print_two("Joshua","Blewitt")
print_two_again("Joshua","Blewitt")
print_one("First!")
print_none()